# Omeka-S-theme-EICAS

This is a branch of https://github.com/omeka-s-themes/default
adapted for EICAS.

See it in action at:
* https://bibliotheek.eicas.nl
* https://collectie.eicas.nl

## Updating

We want to track changes to the 'upstream' theme, to make it
easier to contribute back any generic improvements.

To do this, merge any new upstream tags into this repository.
This will likely cause conflicts, some of which can be resolved
manually, but `asset/css/style.css` in one big blob. To regenerate
that one, `npm install` and `node_modules/.bin/gulp css`.

## Copyright

Copyright © 2021-present EICAS
Copyright © 2016-present Corporation for Digital Scholarship, Vienna, Virginia, USA http://digitalscholar.org

EICAS distributes this branch of the default theme
under the GNU General Public License, version 3 (GPLv3). The full text
of this license is given in the license file.

The Corporation for Digital Scholarship distributes the Omeka source code
under the GNU General Public License, version 3 (GPLv3). The full text
of this license is given in the license file.

The Omeka name is a registered trademark of the Corporation for Digital Scholarship.

Third-party copyright in this distribution is noted where applicable.

All rights not expressly granted are reserved.
